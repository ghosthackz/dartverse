# This is just an example template. You can change this all you like.

import sys
import argparse

def main(sargs):
	parser = argparse.ArgumentParser()
	parser.add_argument('echo', help='What you want the command to echo back.')
	args = parser.parse_args(sargs)
	
	print('Echoing back: '+args.echo)

if __name__ == '__main__':
	main(sys.argv[1:])